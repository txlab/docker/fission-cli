FROM ubuntu:20.04

# renovate: datasource=github-releases depName=fission-suite/fission
ARG FISSION_VERSION=2.18.0

RUN apt-get update && \
    apt-get install -y curl libssl-dev ca-certificates netbase && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN curl -L https://github.com/fission-suite/fission/releases/download/$FISSION_VERSION/fission-cli-ubuntu-20.04-x86_64 \
    -o /usr/local/bin/fission \
    && chmod +x /usr/local/bin/fission

COPY scripts/ /scripts/
ENV PATH /scripts:$PATH

# TODO: pre-download IPFS
# -> turns out, `fission setup` doesn't care, it downloads it either way
# RUN fission setup -k "DUMMY" \
#     && rm -rf ~/.config/fission/key/*

ENTRYPOINT ["entrypoint.sh"]
CMD ["bash"]