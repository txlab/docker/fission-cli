# ARCHIVED - because [fission said farewell](https://web.archive.org/web/20240604174305/https://fission.codes/blog/farewell-from-fission/) 😭


[![size](https://badgen.net/docker/size/tennox/fission-cli)](https://hub.docker.com/r/tennox/fission-cli)
[![last commit](https://badgen.net/gitlab/last-commit/txlab/docker/fission-cli)](https://gitlab.com/txlab/docker/fission-cli/activity)

# Docker image with [fission suite](https://fission.codes/)'s [cli](https://guide.fission.codes/developers/cli)

- Ubuntu 20.04 base (matches fission cli release, and with alpine I had glibc problems)
- renovate bot that auto-updates
  - [updates ubuntu base image tag](https://gitlab.com/txlab/docker/fission-cli/-/merge_requests?scope=all&state=all&author_username=txlab_renovate)
  - [fission version](https://gitlab.com/txlab/docker/fission-cli/-/merge_requests?scope=all&state=all&author_username=txlab_renovate)
- [rebuilt weekly](https://gitlab.com/txlab/docker/fission-cli/-/pipelines) with [latest ubuntu packages](https://gitlab.com/txlab/docker/fission-cli/-/blob/main/.gitlab-ci.yml#L9)

## Usage
```bash
# Just open CLI
docker run --rm -it tennox/fission-cli
# Mount working dir and open terminal with fission cli
docker run --rm -it -v $PWD:/app -w /app tennox/fission-cli
```

### Auto-setup
If you set either
- `FISSION_MACHINE_KEY` or
- `FISSION_MACHINE_KEYFILE` (a path inside the docker container)

The fission CLI will be set up automatically.

For details on those keys check the docs for the [official GitHub action](https://github.com/fission-suite/publish-action#machine_key)

```bash
docker run --rm -it -e FISSION_MACHINE_KEY=$(base64 ~/.config/fission/key/machine_id.ed25519) tennox/fission-cli
```

### One-line publish
- working dir needs `fission.yaml` (run `fission app register`)
```bash
docker run --rm -it -v $PWD:/app -w /app \
    -e FISSION_MACHINE_KEY=$(base64 ~/.config/fission/key/machine_id.ed25519) \
    tennox/fission-cli:latest fission up
```

### GitLab CI
- set `FISSION_MACHINE_KEY` (or `...KEYFILE`) as a Secret Variable
```yaml
deploy fission:
  stage: deploy
  image: tennox/fission-cli:latest
  script:
    - fission up
```