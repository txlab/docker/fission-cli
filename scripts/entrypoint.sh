#!/usr/bin/env bash
set -euo pipefail

# KEY env?
if [[ -n "${FISSION_MACHINE_KEY:-}" ]] || [[ -n "${FISSION_MACHINE_KEYFILE:-}" ]]; then
    setup.sh
    echo # newline
fi

exec "$@"
