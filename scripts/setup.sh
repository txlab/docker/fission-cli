#!/usr/bin/env bash
set -euo pipefail

# KEY env?
if [[ -n "${FISSION_MACHINE_KEY:-}" ]]; then
    echo "$FISSION_MACHINE_KEY" | base64 -d > /tmp/machine_id
    FISSION_MACHINE_KEYFILE=/tmp/machine_id
fi

# KEYFILE env?
if [[ -z "${FISSION_MACHINE_KEYFILE:-}" ]]; then
    echo "Missing env FISSION_MACHINE_KEY or FISSION_MACHINE_KEYFILE" >&2
    exit 1
fi
if [[ ! -f "${FISSION_MACHINE_KEYFILE:-}" ]]; then
    echo "FISSION_MACHINE_KEYFILE is set but the path does not exist: ${FISSION_MACHINE_KEYFILE}" >&2
    exit 1
fi

fission setup -k "$FISSION_MACHINE_KEYFILE"